# Use an official Python runtime as a parent image
FROM backstopjs/backstopjs:4.1.9

# Switch to root user
USER root

# Make backstop-crawl globally available. Basically the only thing doing in this image.
RUN npm install -g aaronbauman/backstop-crawl
